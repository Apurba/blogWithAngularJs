var myApp=angular.module('myMdl',[]);


myApp.controller('myCtrl',function($scope){
    
    
    $scope.newBlog={};
    $scope.clickedCategory={};
    $scope.clickedBlog={};
    
    
    $scope.categories=[
        
        {"id":0,"name":'design'},
        {"id":1,"name":'development'},
        {"id":2,"name":'frontEnd'},
        {"id":3,"name":'backEnd'}
    ];
    
    
$scope.blogs=[
    {'id':0,'title':'AngularJs','url':'https://angularjs.org/', 'detail':'AngularJS is a JavaScript framework. It is a library written in JavaScript....' ,'category':'frontEnd'},
    {'id':1,'title':'php','url':'https://en.wikipedia.org/wiki/PHP','detail':'PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages..' ,'category':'development'},
    {'id':2,'title':'laravel','url':'https://laravel.com/','detail':'The Laravel Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..' ,'category':'frontEnd'},
    {'id':3,'title':'bootstrap','url':'https://getbootstrap.com/','detail':'The Laravel Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..','category':'design'},
    {'id':5,'title':'java','url':'https://www.java.com/en/','detail':'The java Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..','category':'backEnd'},
    {'id':6,'title':'javascript','url':'https://www.javascript.com/','detail':'The javascript Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..','category':'frontEnd'},
    {'id':7,'title':'A.Net','url':'https://www.microsoft.com/net/','detail':'The A.net Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..','category':'development'},
    {'id':8,'title':'css','url':'https://www.w3schools.com/css/','detail':'The css Ecosystem. Revolutionize how you build the web. Instant PHP Platforms On Linode, DigitalOcean, and more..','category':'design'}
]  ;  
    
    
    
    
    $scope.runningCategory=function(category){
        
        $scope.clickedCategory=category;
    };
    
    
    
    $scope.save= function(){
      
        $scope.blogs.push($scope.newBlog);
         $scope.newBlog={};
    };
    
    
    
    $scope.targetBlog=function(blog){
        
        $scope.clickedBlog=blog;
     };
    
    $scope.deleteBlog= function(){
    
         debugger; $scope.blogs.splice($scope.blogs.indexOf($scope.clickedBlog),1);
    };
    
    
    
    
    
    
});